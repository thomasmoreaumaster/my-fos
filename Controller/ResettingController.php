<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FOS\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller managing the resetting of the password
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class ResettingController extends Controller {

    /**
     * Request reset user password: show form
     */
    public function requestAction() {
        return $this->render('FOSUserBundle:Resetting:request.html.twig');
    }

    /**
     * Request reset user password: submit form and send email
     */
    public function sendEmailAction(Request $request) {
        $username = $request->request->get('username');

        /** @var $user UserInterface */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($username);

        if (null === $user) {
            $this->get('session')->getFlashBag()->add('warning', 'Compte inexistant !');
            return $this->redirect($this->generateUrl('homepage'));
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            $this->get('session')->getFlashBag()->add('warning', 'Une demande à déjà été effectué !');
            return $this->redirect($this->generateUrl('homepage'));
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        //$this->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);
        $message = new \Swift_Message('Vous avez demandé à changer de mot de passe ?');

        $message->setBody("<center>" .
                "<table border='0' align='center' width='100%' cellspacing='0' cellpadding='0' height='100%'>"
                . "<tbody><tr><td align='center' valign='top'>" .
                '<span style="color:#ffffff;display:none;font-size:0px;height:0px;width:0px">' .
                "C'est presque fini !" .
                '</span>' .
                '<table border="0" width="100%" cellspacing="0" cellpadding="0">' .
                '  <tbody><tr>' .
                '<td style="background-color:#52bad5;padding-right:30px;padding-left:30px" bgcolor="#52BAD5" align="center" valign="top">' .
                '<table style="max-width:400px" border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                '<tbody><tr>' .
                '<td  align="center" valign="top">' .
                '<img alt="Agicite" src="https://www.agicite.fr/icones/lbleu.png"'
                . ' style="color:#ffffff;margin:0;text-align:center" width="200" height="200">' .
                '</td></tr></tbody></table></td></tr><tr>' .
                '<td style="background-color:#52bad5;padding-right:30px;padding-left:30px" bgcolor="#52BAD5" align="center" valign="top">' .
                '<table border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                '<tbody><tr>' .
                '<td align="center" valign="top"><table style="max-width:640px" border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                '<tbody><tr><td align="center" valign="top">' .
                '<table style="background-color:#ffffff;border-collapse:separate;border-top-left-radius:4px;border-top-right-radius:4px" border="0" bgcolor="#FFFFFF" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                '<tbody><tr>' .
                '<td style="padding-top:40px;padding-bottom:0" align="center" width="100%" valign="top">&nbsp;' .
                '</td></tr></tbody></table></td></tr>' .
                '</tbody></table>' .
                '</td>' .
                '</tr>' .
                '</tbody></table>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td align="center" valign="top">' .
                '<table border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                '<tbody><tr>' .
                '<td align="center" valign="top">' .
                '<table style="max-width:700px" border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                '<tbody><tr>' .
                '<td align="right" width="30" valign="top">' .
                '</td>' .
                '<td style="padding-right:70px;padding-left:40px" width="100%" valign="top">' .
                '<table border="0" align="left" width="100%" cellspacing="0" cellpadding="0">' .
                '<tbody><tr>' .
                '<td style="padding-bottom:40px" align="left" valign="top">' .
                '<h1 style="color:#606060;margin:0;padding:0;text-align:center">' .
                $user->getUsername().' vous souhaitez changer de mot de passe ?</h1>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding-bottom:5px" align="center" valign="top">' .
                '<table border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                '<tbody><tr>' .
                '<td align="center" valign="middle">' .
                '<a href="https://www.agicite.fr/resetting/reset/' . $user->getConfirmationToken() . '" style="background-color:#52bad5;border-collapse:separate;border-top:20px solid #52bad5;border-right:20px solid #52bad5;border-bottom:20px solid #52bad5;border-left:20px solid #52bad5;border-radius:3px;color:#ffffff;display:inline-block;' .
                "font-family:'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:16px;font-weight:600;letter-spacing:.3px;" .
                'text-decoration:none" target="_blank" >Changer mon mot de passe</a>' .
                '</td>' .
                '</tr>' .
                '</tbody></table>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding-bottom:40px" align="left" valign="top">' .
                '<p style="color:#929292;' .
                "font-family:'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;font-style:normal;" .
                'font-weight:400;line-height:42px;letter-spacing:normal;margin:0;padding:0;text-align:center">' .
                "(Vous serez redirigé vers la plate-forme.)</p>" .
                '</td>' .
                '</tr>' .
                '</tbody></table>' .
                '</td>' .
                '</tr>' .
                '</tbody></table>' .
                '</td>' .
                '</tr>' .
                '</tbody></table>' .
                '</td>' .
                '</tr>' .
                '<tr>' .
                '<td style="padding-right:30px;padding-left:30px" align="center" valign="top">' .
                '<table style="max-width:640px" border="0" align="center" width="100%" cellspacing="0"' .
                'cellpadding="0">' .
                '<tbody><tr>' .
                '<td ' .
                'style="border-top:2px solid #f2f2f2;color:#484848;' .
                "font-family:'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:12px;" .
                'font-weight:400;line-height:24px;padding-top:40px;padding-bottom:20px;text-align:center" valign="top">' .
                '<p style="color:#484848;' .
                "font-family:'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;" .
                'line-height:24px;padding:0;margin:0;text-align:center">' .
                '© Agicité<sup>®</sup>, ' .
                'Tous droits réservés.<br>3 rue Delesalle • 59260 Hellemmes • France</p>' .
                '<a href=""' .
                'style="color:#373737;text-decoration:underline" target="_blank" data-saferedirecturl="">Nous contacter</a>' .
                '<span> &nbsp; • &nbsp; </span>' .
                "<a href='' style='color:#373737;text-decoration:underline' target='_blank' data-saferedirecturl=''>Conditions d'utilisation</a>" .
                '<span> &nbsp;' .
                '• &nbsp;' .
                '</span>' .
                '<a href="" style="color:#373737;text-decoration:underline" target="_blank" data-saferedirecturl="">Confidentialité</a>' .
                '</td>' .
                '</tr>' .
                '</tbody></table>' .
                '</td>' .
                '</tr>' .
                '</tbody></table>' .
                '</td>' .
                '</tr>' .
                '</tbody></table>' .
                "</center>", 'text/html');
        //       $message->setBody('<h1>Vous avez demandé à changer de mot de passe ?</h1>'
        //             . '<a target="_blank" href="https://www.agicite.fr/resetting/reset/' . $user->getConfirmationToken() . '">'
        //   . 'Alors suivez ce lien</a>'
        // . '<h3>Vous serez redirigé vers la plateforme afin de changer de mot de passe</h3>', 'text/html');
        $message->addTo($user->getEmail())->addFrom('agicite@agicite.fr');
        $this->get('mailer')->send($message);
        $this->get('session')->getFlashBag()->add('warning', 'un email vous a été envoyé');

        return $this->redirect($this->generateUrl('homepage'));
//return new RedirectResponse($this->generateUrl('fos_user_resetting_check_email',
        //          array('email' => $this->getObfuscatedEmail($user))
        //    ));
    }

    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction(Request $request) {
        $email = $request->query->get('email');

        if (empty($email)) {
            // the user does not come from the sendEmail action
            $this->get('session')->getFlashBag()->add('warning', $this->generateUrl('fos_user_resetting_request'));
            return $this->redirect($this->generateUrl('homepage'));
        }
        $this->get('session')->getFlashBag()->add('warning', $this->generateUrl('fos_user_resetting_request'));
        $this->get('session')->getFlashBag()->add('warning', $email);
        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * Reset user password
     */
    public function resetAction(Request $request, $token) {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.resetting.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_profile_show');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::RESETTING_RESET_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            $this->get('session')->getFlashBag()->add('info', 'Votre compte a bien été modifié :)');
            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('Resetting/reset.html.twig', array(
                    'token' => $token,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Get the truncated email displayed when requesting the resetting.
     *
     * The default implementation only keeps the part following @ in the address.
     *
     * @param \FOS\UserBundle\Model\UserInterface $user
     *
     * @return string
     */
    protected function getObfuscatedEmail(UserInterface $user) {
        $email = $user->getEmail();
        if (false !== $pos = strpos($email, '@')) {
            $email = '...' . substr($email, $pos);
        }

        return $email;
    }

}
