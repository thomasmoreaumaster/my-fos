<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace FOS\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller managing the registration
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class RegistrationController extends Controller {

    public function registerAction(Request $request) {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
            }
            if (null === $user->getConfirmationToken()) {
                /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
                $tokenGenerator = $this->get('fos_user.util.token_generator');
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }
            $this->get('fos_user.user_manager')->updateUser($user);
            $message = new \Swift_Message('Bienvenue ' . $user->getUsername() . ' sur Agicité');
            //$message->setBody('<h1>Bienvenue ' . $user->getUsername() . ' sur Agicité</h1>'
            //      . 'Pour des raisons de sécurité vous devez cliquer sur ce lien afin de valider votre compte.'
            //    . '<br/><a target="_blank" href="https://www.agicite.fr/register/confirm/' . $user->getConfirmationToken() . '">'
            //  . ' Je confirme mon inscription !</a>'
            //. '<h3>Vous serez redirigé vers la plateforme</h3>', 'text/html');
            $message->setBody("<center>" .
                    "<table border='0' align='center' width='100%' cellspacing='0' cellpadding='0' height='100%'>"
                    . "<tbody><tr><td align='center' valign='top'>" .
                    '<span style="color:#ffffff;display:none;font-size:0px;height:0px;width:0px">' .
                    "Premier pas sur Agicité" .
                    '</span>' .
                    '<table border="0" width="100%" cellspacing="0" cellpadding="0">' .
                    '  <tbody><tr>' .
                    '<td style="background-color:#52bad5;padding-right:30px;padding-left:30px" bgcolor="#52BAD5" align="center" valign="top">' .
                    '<table style="max-width:400px" border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                    '<tbody><tr>' .
                    '<td  align="center" valign="top">' .
                    '<img alt="Agicite" src="https://www.agicite.fr/icones/lbleu.png"'
                    . ' style="color:#ffffff;margin:0;text-align:center" width="200" height="200">' .
                    '</td></tr></tbody></table></td></tr><tr>' .
                    '<td style="background-color:#52bad5;padding-right:30px;padding-left:30px" bgcolor="#52BAD5" align="center" valign="top">' .
                    '<table border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                    '<tbody><tr>' .
                    '<td align="center" valign="top"><table style="max-width:640px" border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                    '<tbody><tr><td align="center" valign="top">' .
                    '<table style="background-color:#ffffff;border-collapse:separate;border-top-left-radius:4px;border-top-right-radius:4px" border="0" bgcolor="#FFFFFF" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                    '<tbody><tr>' .
                    '<td style="padding-top:40px;padding-bottom:0" align="center" width="100%" valign="top">&nbsp;' .
                    '</td></tr></tbody></table></td></tr>' .
                    '</tbody></table>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td align="center" valign="top">' .
                    '<table border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                    '<tbody><tr>' .
                    '<td align="center" valign="top">' .
                    '<table style="max-width:700px" border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                    '<tbody><tr>' .
                    '<td align="right" width="30" valign="top">' .
                    '</td>' .
                    '<td style="padding-right:70px;padding-left:40px" width="100%" valign="top">' .
                    '<table border="0" align="left" width="100%" cellspacing="0" cellpadding="0">' .
                    '<tbody><tr>' .
                    '<td style="padding-bottom:40px" align="left" valign="top">' .
                    '<h1 style="color:#606060;margin:0;padding:0;text-align:center">' .
                    'Bienvenue ' . $user->getUsername() . ' sur Agicité</h1>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom:5px" align="center" valign="top">' .
                    '<table border="0" align="center" width="100%" cellspacing="0" cellpadding="0">' .
                    '<tbody><tr>' .
                    '<td align="center" valign="middle">' .
                    '<a href="https://www.agicite.fr/register/confirm/' . $user->getConfirmationToken() . '" style="background-color:#52bad5;border-collapse:separate;border-top:20px solid #52bad5;border-right:20px solid #52bad5;border-bottom:20px solid #52bad5;border-left:20px solid #52bad5;border-radius:3px;color:#ffffff;display:inline-block;' .
                    "font-family:'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:16px;font-weight:600;letter-spacing:.3px;" .
                    'text-decoration:none" target="_blank" >Valider mon inscription</a>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-bottom:40px" align="left" valign="top">' .
                    '<p style="color:#929292;' .
                    "font-family:'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:14px;font-style:normal;" .
                    'font-weight:400;line-height:42px;letter-spacing:normal;margin:0;padding:0;text-align:center">' .
                    "(Confirmez que c'est bien vous.)</p>" .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>' .
                    '</td>' .
                    '</tr>' .
                    '<tr>' .
                    '<td style="padding-right:30px;padding-left:30px" align="center" valign="top">' .
                    '<table style="max-width:640px" border="0" align="center" width="100%" cellspacing="0"' .
                    'cellpadding="0">' .
                    '<tbody><tr>' .
                    '<td ' .
                    'style="border-top:2px solid #f2f2f2;color:#484848;' .
                    "font-family:'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:12px;" .
                    'font-weight:400;line-height:24px;padding-top:40px;padding-bottom:20px;text-align:center" valign="top">' .
                    '<p style="color:#484848;' .
                    "font-family:'Open Sans','Helvetica Neue',Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;" .
                    'line-height:24px;padding:0;margin:0;text-align:center">' .
                    '© Agicité<sup>®</sup>, ' .
                    'Tous droits réservés.<br>3 rue Delesalle • 59260 Hellemmes • France</p>' .
                    '<a href=""' .
                    'style="color:#373737;text-decoration:underline" target="_blank" data-saferedirecturl="">Nous contacter</a>' .
                    '<span> &nbsp; • &nbsp; </span>' .
                    "<a href='' style='color:#373737;text-decoration:underline' target='_blank' data-saferedirecturl=''>Conditions d'utilisation</a>" .
                    '<span> &nbsp;' .
                    '• &nbsp;' .
                    '</span>' .
                    '<a href="" style="color:#373737;text-decoration:underline" target="_blank" data-saferedirecturl="">Confidentialité</a>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>' .
                    '</td>' .
                    '</tr>' .
                    '</tbody></table>' .
                    "</center>", 'text/html');

            $message->addTo($user->getEmail())->addFrom('agicite@agicite.fr');
            $this->get('mailer')->send($message);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            $this->get('session')->getFlashBag()->add('info', "Un email vous a été envoyé afin d'activer votre compte ! Consultez vite vos mails.");
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(array('iduser' => $user->getId())); // appel AJAX Lors de la creation d'une asso ou request proprio
            } else {
                return $this->redirect($this->generateUrl('homepage'));
            }
        } else {
            $this->get('session')->getFlashBag()->add('info', "Une erreur s'est produite");
            return $this->redirect($this->generateUrl('homepage'));
        }

        return $this->render('FOSUserBundle:Registration:register.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction() {
        $email = $this->get('session')->get('fos_user_send_confirmation_email/email');
        $this->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->get('fos_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));
        }

        return $this->render('FOSUserBundle:Registration:checkEmail.html.twig', array(
                    'user' => $user,
        ));
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction(Request $request, $token) {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);

        $userManager->updateUser($user);

        $message = new \Swift_Message('Agicité Support');
        $message->setBody('<h1>Bienvenue ' . $user->getUsername() . ' sur Agicité</h1>
<h4>Votre compte à bien été enregistré, vous pouvez désormais commencer à participer à la vie de votre commune !</h4>
<h4>Pour cela connectez-vous et recherchez votre commune, vous pourrez alors commencer à débattre et voter sur les projets en cours et même proposer vos idées !</h4>
<h5>Si vous êtes directeur ou gérant d\'une association, enregistrez la tout de suite depuis votre profil.</h5>
<a href="https://www.agicite.fr/"> Merci et à trés vite sur Agicité</a><br />
<a href="https://www.agicite.fr/site/index.html"> Plus d\'informations sur le rôle d\'un ambassadeur ou sur l\'espace associatif ici !</a>
', 'text/html');
        $message->addTo($user->getEmail())->addFrom('contact@agicite.fr');
        $this->get('mailer')->send($message);

        $this->get('session')->getFlashBag()->add('info', "Votre compte est maintenant valide ! Connectez-vous vite.");

        return $this->redirect($this->generateUrl('homepage'));

        if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('fos_user_registration_confirmed');
            $response = new RedirectResponse($url);
        }

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }

    /**
     * Tell the user his account is now confirmed
     */
    public function confirmedAction() {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('FOSUserBundle:Registration:confirmed.html.twig', array(
                    'user' => $user,
                    'targetUrl' => $this->getTargetUrlFromSession(),
        ));
    }

    private function getTargetUrlFromSession() {
        // Set the SecurityContext for Symfony <2.6
        if (interface_exists('Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface')) {
            $tokenStorage = $this->get('security.token_storage');
        } else {
            $tokenStorage = $this->get('security.context');
        }

        $key = sprintf('_security.%s.target_path', $tokenStorage->getToken()->getProviderKey());

        if ($this->get('session')->has($key)) {
            return $this->get('session')->get($key);
        }
    }

}
